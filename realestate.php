<?php
/**
 * Plugin Name: Real Estate
 */
function real_estate_sample_shortcode() {
    wp_enqueue_style('real-estate-css');
    wp_enqueue_script('babel', 'https://unpkg.com/babel-standalone@6/babel.min.js' );
    wp_enqueue_script('axios', 'https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.min.js' );
    wp_enqueue_script('vue', 'https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js' );
    wp_enqueue_script('vuex', 'https://cdnjs.cloudflare.com/ajax/libs/vuex/3.1.1/vuex.min.js' );
    wp_enqueue_script('index', plugin_dir_url( __FILE__ ) . 'js/index.js', 1);
    $real_estate_url = plugin_dir_url( __FILE__ ) . 'js/response.json';
    return "
        <script>
            window.realEstateUrl = '$real_estate_url';
        </script>
        <div id='app'></div>
    ";
}
wp_register_style( 'real-estate-css', plugin_dir_url( __FILE__ ) . 'css/index.css' );
add_shortcode( 'real_estate_sample_shortcode', 'real_estate_sample_shortcode' );

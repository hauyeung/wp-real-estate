const template = `
<div>
    <div v-for="l in listings" class="listing">
        <div class="left">
            <img :src="l.Property.Photo[0].HighResPath" />
        </div>
        <div class="right">
            <p>{{l.Property.Price}}</p>
            <p>{{l.Property.Address.AddressText}}</p>
            <p>
                {{l.Building.Bedrooms}} bd | 
                {{l.Building.BathroomTotal}} ba | 
                {{l.Building.SizeInterior}} |
                {{l.Land.SizeTotal}}
            </p>
            <p>
                {{l.Building.Type}}
            </p>
        </div>
    </div>
</div>
`;
const app = new Vue({
    el: '#app',
    data: {
        listings: []
    },
    created: function () {
        axios
            .get(window.realEstateUrl)
            .then((response) => {
                this.listings = response.data;
            })
    },
    template
})